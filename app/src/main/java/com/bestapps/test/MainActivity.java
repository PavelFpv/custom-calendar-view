package com.bestapps.test;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends FragmentActivity {

    private CustomCalendarView calendarView;
    private List<Event> mItems;
    private EventsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAdapter();
        calendarView = (CustomCalendarView) findViewById(R.id.customCalendarView);
        calendarView.setPeriod("week");
        calendarView.setAdapter(mAdapter);
    }

    private void initAdapter() {
        mItems = new ArrayList<>();

        ArrayList<EventDate> events = new ArrayList<>();
        events.add(new EventDate(1,1,new Date(),true));
        events.add(new EventDate(1,5,new Date(),true));
        events.add(new EventDate(1,10,new Date(),true));

        mItems.add(new Event(1, events, new String("Wake up at 9")));
        mItems.add(new Event(2, events, new String("Do exercises every day")));
        mItems.add(new Event(3, events, new String("Walk to working place")));
        mItems.add(new Event(4, events, new String("Doing hard on work")));
        mItems.add(new Event(5, events, new String("Take dinner at 13")));
        mItems.add(new Event(6, events, new String("Go to the gym at 20")));
        mItems.add(new Event(7, events, new String("Go to sleep at 23")));
        mItems.add(new Event(8, events, new String("Do exercises fo eyes every two hours")));
        mItems.add(new Event(9, events, new String("Go to the country on holidays")));
        mItems.add(new Event(10, events, new String("Run morning on holidays")));

        mAdapter = new EventsAdapter(this, mItems);
    }
}
