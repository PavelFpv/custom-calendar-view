package com.bestapps.test;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel on 18.05.2015.
 */
public class EventsAdapter extends BaseAdapter {
    private LayoutInflater lInflater;
    private List<Event> objects;

    public EventsAdapter(Activity context, List<Event> events) {
        objects = events;
        lInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private static class ViewHolder {
        public TextView eventName;
        public CheckBox eventMonday;
        public CheckBox eventTuesday;
        public CheckBox eventWednesday;
        public CheckBox eventThursday;
        public CheckBox eventFriday;
        public CheckBox eventSaturday;
        public CheckBox eventSunday;
    }

    public void addItem(Event item){
        objects.add(item);
        notifyDataSetChanged();
    }

    public void deleteItem(Event item){
        objects.remove(item);
        notifyDataSetChanged();
    }

    public ArrayList<Event> getItems(){
        return (ArrayList)objects;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View view = convertView;

        if (view == null) {
            view = lInflater.inflate(R.layout.event_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.eventName = (TextView) view.findViewById(R.id.event_name);
            viewHolder.eventMonday = (CheckBox) view.findViewById(R.id.event_monday);
            viewHolder.eventTuesday = (CheckBox) view.findViewById(R.id.event_tuesday);
            viewHolder.eventWednesday = (CheckBox) view.findViewById(R.id.event_wednesday);
            viewHolder.eventThursday = (CheckBox) view.findViewById(R.id.event_thursday);
            viewHolder.eventFriday = (CheckBox) view.findViewById(R.id.event_friday);
            viewHolder.eventSaturday = (CheckBox) view.findViewById(R.id.event_saturday);
            viewHolder.eventSunday= (CheckBox) view.findViewById(R.id.event_sunday);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Event event = ((Event) getItem(position));

        viewHolder.eventName.setText(event.getName());

        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

}
