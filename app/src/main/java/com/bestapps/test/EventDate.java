package com.bestapps.test;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Pavel on 22.05.2015.
 */
public class EventDate  implements Serializable {
    private static final long serialVersionUID = 0L;
    private int id;
    private int event_id;
    private Date date;
    private boolean status;

    public EventDate(int id, int event_id, Date date, boolean status) {
        this.id = id;
        this.event_id = event_id;
        this.date = date;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
