package com.bestapps.test;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Pavel on 18.05.2015.
 */
public class PagerFragment extends Fragment {
    public static final String ITEMS = "items";
    public static int listViewPos = 0;

    private ListView eventList;

    public static PagerFragment create(ArrayList<Event> events) {
        PagerFragment fragment = new PagerFragment();
        Bundle args = new Bundle();
        args.putSerializable(ITEMS, events);
        fragment.setArguments(args);
        return fragment;
    }

    public PagerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.events_list, container, false);

        ArrayList<Event> items = (ArrayList)this.getArguments().getSerializable(ITEMS);

        // Set the title view to show the page number.
        eventList = (ListView) rootView.findViewById(R.id.events_listView);
        eventList.setAdapter(new EventsAdapter(getActivity(),items));
        eventList.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int currentScrollState;
            private int currentVisibleItemCount;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
                this.currentVisibleItemCount = visibleItemCount;
            }

            private void isScrollCompleted() {
                if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
                    /*** In this way I detect if there's been a scroll which has completed ***/
                    /*** do the work! ***/
                    listViewPos = eventList.getFirstVisiblePosition();
                }
            }

        });

        return rootView;
    }

}