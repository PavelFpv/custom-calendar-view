package com.bestapps.test;

import android.os.Bundle;
import android.util.Log;

/**
 * Created by Pavel on 18.05.2015.
 */
public class Sets {
    final String LOG_TAG = "myLogs";

    int a[]={18897109, 12828837, 9461105, 6371773, 5965343, 5946800, 5582170, 5564635, 5268860, 4552402,
            4335391, 4296250, 4224851, 4192887, 3439809, 3279833, 3095313, 2812896, 2783243, 2710489,
            2543482, 2356285, 2226009, 2149127, 2142508, 2134411};
    int power=26;

    //calculate();
    //startRecur();


    void calculate() {

        int iter = (int)Math.pow(2,power);
        int sum = 0;
        String result="";

        long time, time2;
        time = System.currentTimeMillis();
        for (int i=0; i<iter; i++) {
            sum = 0;
            result = "";
            for (int j=0; j<power; j++)
                if ((i&(1<<j))>0) {
                    sum += a[j];
                }
            if (sum == 100000000) {
                for (int j=0; j<power; j++)
                    if ((i&(1<<j))>0)
                        result = result + ", " + String.valueOf(a[j]);
                break;
            }
        }
        time2 = System.currentTimeMillis();
        time = (time2 - time) / 1000;

        Log.d(LOG_TAG, result);
        Log.d(LOG_TAG, "sum="+String.valueOf(sum) + " time=" + String.valueOf(time));
    }

    void recursionFunc(int[] binArr, int power, int pos){
        int sum=0;
        String result="";

        if (power == pos) {
            for (int i=0; i < power; i++) {
                if (binArr[i] == 1)
                    sum += a[i];
            }
            if (sum == 100000000) {
                for (int i=0; i < power ; i++) {
                    if (binArr[i] == 1)
                        result = result + ", " + String.valueOf(a[i]);
                }
                Log.d(LOG_TAG, "sum= "+String.valueOf(sum));
                Log.d(LOG_TAG, "result= "+result);
            }
        }
        else {
            binArr[pos] = 0;
            recursionFunc(binArr, power, pos+1);

            binArr[pos] = 1;
            recursionFunc(binArr, power, pos+1);
        }
    }

    void startRecur() {
        int binArr[]={0,0,0,0,0,0,0,0,0,0,
                0,0,0,0,0,0,0,0,0,0,
                0,0,0,0,0,0};
        int startPos=0;
        long time, time2;

        time = System.currentTimeMillis();
        recursionFunc(binArr, power, startPos);
        time2 = System.currentTimeMillis();
        time = (time2 - time) / 1000;

        Log.d(LOG_TAG, "time=" + String.valueOf(time));
    }



    private void sort() {
        for(int i = 0; i < a.length - 1; i++)
            for(int j = 0; j < a.length - i - 1; j++)
                if(a[j] > a[j + 1]) {
                    int temp;
                    temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
    }
}
