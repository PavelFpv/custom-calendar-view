package com.bestapps.test;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Pavel on 18.05.2015.
 */
public class CustomCalendarView extends RelativeLayout {
    private static final String MONTH = "month";
    private static final String WEEK = "week";

    private ImageView prev, next;
    private TextView period;
    private LinearLayout week_layout;
    private String calendarMode;
    private ViewPager mPager;
    private ArrayList<Event> mItems;
    private ArrayList<String> mDates;
    private int todayPos=0;

    public CustomCalendarView(Context context) {
        this(context,null);
    }

    public CustomCalendarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomCalendarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        ((Activity)getContext())
                .getLayoutInflater()
                .inflate(R.layout.calendar_view, this, true);

        if (attrs != null) {
            TypedArray a = getContext()
                    .obtainStyledAttributes(attrs, R.styleable.CustomCalendarView, 0,0);
            calendarMode = a.getString(R.styleable.CustomCalendarView_period);
            a.recycle();
        }

        setCalendar();
        setUpViews();
    }

    private void setCalendar() {
        Calendar startDate, endDate, todayDate;
        startDate = Calendar.getInstance();
        endDate = Calendar.getInstance();
        todayDate = Calendar.getInstance();

        startDate.clear();
        endDate.clear();

        startDate.set(1980, 0, 1);
        endDate.set(2100, 0, 1);

        mDates = getList(startDate, endDate);
        String today = getDate(todayDate);

        for(String date : mDates) {
            if (today.equals(date))
                todayPos = mDates.indexOf(date);
        }
    }

    private ArrayList<String> getList(Calendar startDate, Calendar endDate) {
        ArrayList<String> list = new ArrayList<>();

        while (startDate.compareTo(endDate) <= 0) {
            list.add(getDate(startDate));
            if (calendarMode.equals(MONTH))
                startDate.add(Calendar.MONTH, 1);
            else if (calendarMode.equals(WEEK))
                startDate.add(Calendar.WEEK_OF_MONTH, 1);
        }

        return list;
    }

    private String getDate(Calendar cld) {
        String curDate = "";

        if (calendarMode.equals(MONTH)) {
            curDate = getResources().getStringArray(R.array.month)[cld.get(Calendar.MONTH)];
            curDate += " " + cld.get(Calendar.YEAR);
        } else if (calendarMode.equals(WEEK)) {
            Calendar dayWeekStart = Calendar.getInstance();
            Calendar dayWeekEnd = Calendar.getInstance();
            dayWeekStart.clear();
            dayWeekEnd.clear();
            dayWeekStart.setTime(cld.getTime());
            dayWeekEnd.setTime(cld.getTime());

            int delta = cld.get(Calendar.DAY_OF_WEEK);

            dayWeekStart.add(Calendar.DAY_OF_MONTH,-delta+2);
            dayWeekEnd.add(Calendar.DAY_OF_MONTH,8-delta);

            curDate += dayWeekStart.get(Calendar.DAY_OF_MONTH) + " " + (dayWeekStart.get(Calendar.MONTH)==dayWeekEnd.get(Calendar.MONTH)?"":
                    getResources().getStringArray(R.array.part_of_month)[dayWeekStart.get(Calendar.MONTH)] + " ") +
                    (dayWeekStart.get(Calendar.YEAR)==dayWeekEnd.get(Calendar.YEAR)?"":String.valueOf(dayWeekStart.get(Calendar.YEAR)) + " ") +
                    "- " + dayWeekEnd.get(Calendar.DAY_OF_MONTH) +
                    " " + getResources().getStringArray(R.array.part_of_month)[dayWeekEnd.get(Calendar.MONTH)] +
                    " " + dayWeekEnd.get(Calendar.YEAR);
        }

        return curDate;
    }

    private void setUpViews() {
        prev = (ImageView)findViewById(R.id.left_imageView);
        next = (ImageView)findViewById(R.id.right_imageView);

        period = (TextView)findViewById(R.id.calendar_period_textView);
        period.setText(mDates.get(todayPos));

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new ListViewPagerAdapter(((MainActivity) getContext()).getSupportFragmentManager()));
        mPager.setCurrentItem(todayPos);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int all = mPager.getChildCount();
                for(int i=0; i<all; i++) {
                    View v = mPager.getChildAt(i);
                    ListView listView = (ListView) v.findViewById(R.id.events_listView);
                    listView.setSelection(PagerFragment.listViewPos);
                }
            }

            @Override
            public void onPageSelected(int position) {
                period.setText(mDates.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        week_layout = (LinearLayout)findViewById(R.id.week_layout);
    }

    public void setPeriod(String period) {
        calendarMode = period;
    }

    public void setAdapter(EventsAdapter adapter) {
        mItems = adapter.getItems();
    }


    private class ListViewPagerAdapter extends FragmentStatePagerAdapter {
        public ListViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PagerFragment.create(mItems);
        }

        @Override
        public int getCount() {
            return mDates.size();
        }
    }
}