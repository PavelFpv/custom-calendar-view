package com.bestapps.test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Pavel on 18.05.2015.
 */
public class Event implements Serializable {
    private static final long serialVersionUID = 0L;

    private int id;
    private ArrayList<EventDate> events;
    private String name;

    public Event(int id, ArrayList<EventDate> events, String name) {
        this.id = id;
        this.events = events;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<EventDate> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<EventDate> events) {
        this.events = events;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
